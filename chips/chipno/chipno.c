#include <avr/interrupt.h>    // Defines pins, ports, etc to make programs easier to read
#define F_CPU 1000000UL	      // Sets up the default speed for delay.h
#include <util/delay.h>
#include <stdlib.h>
#include <math.h>

int main(){
  uint8_t but1, but1a, oldBut, count;
  count = 0;
  uint16_t count2 = 1, count3 = 1;

  // setup output pins
  DDRD = _BV(PD4) | _BV(PD3) | _BV(PD2);
  DDRA = _BV(PA1);
  DDRB = _BV(PB3) | _BV(PB2);

  TCCR0A = _BV(COM0A0) | _BV(WGM01);

  /*
    TCCR0B = Timer/Counter 0 control register B
    CS01:0 = sets prescaler
  */
  TCCR0B =  _BV(CS01) | _BV(CS00);

  /*
    TCCR1A = Timer/Counter 1 control register A
    COM1A0 = Toggle OC1A on compare match (non-PWM)
  */
  TCCR1A = _BV(COM1A0);
  /*
    TCCR1B = Timer/Counter 1 control register B
    WGM12 = CTC mode
    CS11:0 = sets prescaler
  */

  //TCCR1B = _BV(WGM12) | _BV(CS11) | _BV(CS10);
  TCCR1B = _BV(WGM12) | _BV(CS12) | _BV(CS10);

  while(1){
    // sets output compare regiater, top.
    //OCR0A = f+(count2*8);
    TCNT0 = 0;
    OCR0A = 1+((count*2)%3);
    TCNT1 = 0;
    OCR1A = 1+((count2*2)%5)+(count3);
    // sets prescaler

    count = (count+1)%16;
    count2 = (count2+1)%4;
    if(count == 0) {count3 = (count3+1)%4;}

    //0 1 2 3 | 4 5 6 7 | 8 9 10 11 | 12 13 14 15
    //*           *         *         *      *
    //    *         *         *           *    
    if(count==0 || count==5 || count==9 || count==12 || count==14) {PORTD |= _BV(PD4);} else {PORTD &= ~_BV(PD4);}
    if(count==2 || count==6 || count==10 || count==13) {PORTD |= _BV(PD3);} else {PORTD &= ~_BV(PD3);}

    if(count3%2 == 0) {PORTA = _BV(PA1);}
    if(count3%2 == 1) {PORTA = ~_BV(PA1);}
    _delay_ms(100);
  }

  return(0);
}
