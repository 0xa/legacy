// An early attempt for an 0xA synth lib

(
SynthDef("jitlib-example", { arg out;
	Out.ar(out, Ringz.ar(WhiteNoise.ar(0.01), 1000*[1,1], 0.001))
}).store; // store the synth def so it is added to the SynthDescLib
)