// =====================================================================
// SuperCollider Workspace
// =====================================================================

// Start audio
s.boot
s = Server.default

p = ProxySpace.push(s)

// Eval with C-c C-c
// We assign a sound function to a variable to allow control over it (see bottom of file)
~out = { SinOsc.ar(900) };
~out.play

//  replace the function
// change volume by multiplying the signal
~out = { SinOsc.ar(440) * 0.1 };
// change volume by changing Ugen parameters
// either specifying values with keywords in the order wanted, or just 
// numbers in the hardcoded order of the synth
~out = { SinOsc.ar(freq:880, phase:0, mul:0.5) };
~out = { SinOsc.ar(1000, 0, 0.2) };
// STOP with C-c C-s

// fadeTime is a method (starts with lower case)
// it replaces one function with another in x seconds
~out.fadeTime = 3


// some other UGens
~out = { Saw.ar(200, 0.5) }

// some filters
// Basic High-Pass Filter
~out = { HPF.ar(in: Saw.ar(200, 0.5), freq: 1000) }
// Same with resonance
~out = { RHPF.ar(in: Saw.ar(200, 0.5), freq: 1000, rq: 0.1) }
// There is also LPF, RLPF and BPF
// Check Filter.sc for class definitions, or type
// C-c :

// Help files
// C-c C-h
// 

// LF = bandlimited 
~out = { LFTri.ar
	(LFCub.kr
		(LFPulse.kr
			(0.02, 0, 8, 10),
			0, 
			400,
			800),
		0,
		0.1
	) 
}

~out = { SyncSaw.ar(50, XLine.kr(100, 1800, 12), 0.7) }
// convert out of a range .range, .linlin see UGen help for more

~out = { SyncSaw.ar(50, XLine.kr(100, 1800, 12), 0.7) }

// DEBUG !!!
~out = { SinOsc.ar(LFNoise0.ar(2).range(420, 460).poll(label: \LFNoise), 0, 0.2) }
~out = { SyncSaw.ar(50, XLine.kr(100, 1800, 12).poll, 0.7) }


//////////////////////////////////////////
// control from the outside
~out = { 
	arg syncfreq = 50, sawfreq = 50, mul = 0.4; 
	SyncSaw.ar(syncfreq, sawfreq, mul) 
}

// or
// to select a whole chunk at once (double click on 1st or last bracket
(
~out = { 
	arg syncfreq = 50, sawfreq = 50, mul = 0.4; 
	SyncSaw.ar(syncfreq, sawfreq, mul) 
}
)

// or
// another way to pass arguments
~out = { 
	|syncfreq = 50, sawfreq = 50, mul = 0.4|
	SyncSaw.ar(syncfreq, sawfreq, mul) 
}

// change the values of the sound function
~out.set(\syncfreq, 11, \sawfreq, 100)


// Same with bit of interpolation for the values
// Example lag for linear interpolation in x sec
// lag3 for cubic interpolation, etc ... see help :)
~out = { 
	|syncfreq = 50, sawfreq = 50, mul = 0.4|
	SyncSaw.ar(syncfreq.lag3(2), sawfreq.lag(10), mul)
}

~out.set(\syncfreq, 110, \sawfreq, 1000)

// Same with filter
~out = { 
	|syncfreq = 50, sawfreq = 50, mul = 0.6, filterfreq = 100|
	RLPF.ar(
		in:	SyncSaw.ar(syncfreq.lag3(0.1), sawfreq.lag(2), mul),
		freq: filterfreq.lag2(3),
		rq: 0.6
	)
}

~out.set(\syncfreq, 110, \sawfreq, 200, \filterfreq, 100)
~out.set(\syncfreq, 11, \sawfreq, 10, \filterfreq, 1000)


// Tdef = named task, task = routine
Tdef(\whow, {
	loop{
		~out.set(\syncfreq, 110, \sawfreq, 200, \filterfreq, 100);
		wait(10);
		~out.set(\syncfreq, 11, \sawfreq, 10, \filterfreq, 1000);
		wait(10)
	}
})

Tdef(\whow).play
Tdef(\whow).stop

//
// post, postln, postcln

Tdef(\whow, {
	inf.do{
		|i|
		~out.set(\syncfreq, (110*i).postln, \sawfreq, 200, \filterfreq, 100);
		wait(2);
		~out.set(\syncfreq, 11, \sawfreq, (10+i*10).postcln, \filterfreq, 5000);
		wait(1)
	}
})

Tdef(\whow).play
Tdef(\whow).stop


//

Tdef(\whow, {|env|
	env.arr = [90, 80, 70, 76].midicps;
	inf.do{
		|i|
		var size = env.arr.size;
		~out.set(\syncfreq, env.arr[i%size], \sawfreq, 200, \filterfreq, 100);
		wait(2);
		~out.set(\syncfreq, env.arr[i%size], \sawfreq, 100, \filterfreq, 4000);
		wait(1)
	}
})

Tdef(\whow).set(\arr, [70, 60, 50, 56].midicps )
Tdef(\whow).set(\arr, [49, 48, 46, 51].midicps )
Tdef(\whow).set(\arr, [49, 45].midicps )


Tdef(\whow).play
Tdef(\whow).stop



// AUDIO CONTROL /////////////////////////////////
// quick mute
~out.stop
~out.play
// or ...
~out.pause
~out.resume
// kill ~out
~out.free
// kill all lang processes
// C-c C-s
// kill server
s.quit